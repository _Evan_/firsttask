<?php

require_once '../application/models/model_auth.php';

class Controller_Auth extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Model_Auth;
    }

    function action_index()
    {
        $this->view->generate('auth_view.php', 'template_view.php',
            array(
                'title' => 'Login page'
            )
        );
    }
    function action_login()
    {
        $this->model->login($_POST);
        header("Location: /");
    }
    function action_logout()
    {
       $this->model->logout();
        header("Location: /");
    }
    //TODO Add realization for registration
    function action_register($data)
    {

    }
}