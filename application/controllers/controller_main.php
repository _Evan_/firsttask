<?php

require_once '../application/models/model_task.php';

class Controller_Main extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Model_Task;
    }

//TODO: Create separate class for pagination

    function action_index()
    {
       $paginator = array();
       $paginator['perPage']=3;
       $paginator['currentPage'] = isset($_GET['page']) ? $_GET['page'] : 1;
       $paginator['offset'] = ($paginator['currentPage']*$paginator['perPage']) - $paginator['perPage'];

        if(is_null($_GET['key']))
            $paginator['key'] = "id";
        else
            $paginator['key'] = $_GET['key'];

       list($tasks,$count) = $this->model->getListModel($paginator['key'],$paginator['offset'],$paginator['perPage']);

       $paginator['pageCnt'] = ceil($count/$paginator['perPage']);

        $this->view->generate('main_view.php', 'template_view.php',
            array(
                'paginator' => $paginator,
                'tasks' => $tasks,
                'title' => 'Main page'
            )
        );
    }

    function action_show($id)
    {
        $task = $this->model->getTask($id);
        $this->view->generate('tasks/show.php', 'template_view.php',
            array(
                'task' => $task,
                'title' => 'Task page',
            )
        );
    }

    function action_create()
    {
        $this->view->generate('tasks/create.php', 'template_view.php',
            array(
                'title' => 'Create page'
            )
        );
    }

    function action_store()
    {
        $this->model->create($_POST);
        header("Location: /");
    }

     function action_edit($id)
     {
         $task = $this->model->getTask($id);
         $this->view->generate('tasks/edit.php', 'template_view.php',
             array(
                 'task' => $task,
                 'title' => 'Edit page'
             )
         );
     }

    function action_update($id)
    {
        $this->model->update($id,$_POST);
        header("Location: /");
    }

    function action_delete($id)
    {
        $this->model->remove($id);
        header("Location: /");
    }
}