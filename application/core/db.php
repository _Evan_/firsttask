<?php

 class DataBase
{
    const USER = "root";
    const PASSWORD = "";
    const HOST = "localhost";
    const DBName = "siteInfo";
    const PORT = "9006";

    public static function connect()
    {
        $user = self::USER;
        $password=self::PASSWORD;
        $host=self::HOST;
        $dbName=self::DBName;
        $port = self::PORT;

        $conn = new PDO("mysql:host=$host;dbname=$dbName;port=$port",$user,$password);
        return $conn;
    }
}