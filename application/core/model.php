<?php

abstract class Model
{
    protected $query;

    public function __construct()
    {
        $this->query = new QueryBuilder();
    }
}