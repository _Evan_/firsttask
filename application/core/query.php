<?php

require_once 'db.php';

class QueryBuilder
{
    public  $pdo =null;

    public function __construct()
    {
        $this->pdo=DataBase::connect();
    }

    public function getAll($table)
    {
        $sql = "SELECT * FROM $table";
        $statement = $this->pdo->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getOne($table,$id)
    {
        $sql = "SELECT * FROM $table WHERE id=:id";
        $statement = $this->pdo->prepare($sql);
        $statement->bindParam(':id',$id);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function  add($table,$data)
    {
        $keys = array_keys($data);
        $stringOfKeys = implode(',',$keys);
        $placeholders = ':'.implode(', :',$keys);
        $sql = "INSERT INTO $table ($stringOfKeys) VALUES ($placeholders)";
        $statement = $this->pdo->prepare($sql);
        $statement->execute($data);
    }

    function update($table,$data){
        $fields = '';
        foreach($data as $key=>$value)
        {
            $fields.=$key."=:".$key.",";
        }
        $fields = rtrim($fields,',');
       $sql = "UPDATE $table SET $fields WHERE id=:id";
        $statement = $this->pdo->prepare($sql);
        $statement->execute($data);
    }

    public function delete($table,$id)
    {
        $sql = "DELETE FROM $table WHERE id=:id";
        $statement = $this->pdo->prepare($sql);
        $statement->bindParam(':id',$id);
        $statement->execute();
    }

    public function  sort($table,$sortString,$offset,$limit)
    {
        $sql = "SELECT * FROM $table ORDER BY $sortString  LIMIT $offset,$limit ";
        $statement = $this->pdo->prepare($sql);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function  getCount($table)
    {
        $sql = "SELECT COUNT(*) AS count FROM $table";
        $statement = $this->pdo->prepare($sql);
        $statement->execute();
        return  $statement->fetch();
    }

}