<?php

class Route
{
    static function start()
    {
        $controller_name = 'Main';
        $action_name = 'index';
        $params = null;

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1]))
        {
            $controller_name = $routes[1];
        }

        if (!empty($routes[2]))
        {
            $action_name = $routes[2];
        }

        if(!empty(array_slice($routes, 3)))
        {
            $params = array_slice($routes, 3);
        }

        $controller_name = 'Controller_' . $controller_name;
        $action_name = 'action_' . $action_name;

        $controller_file = strtolower($controller_name) . '.php';
        $controller_path = "../application/controllers/" . $controller_file;

        if (file_exists($controller_path))
        {
            include "../application/controllers/" . $controller_file;
        } else {
            Route::ErrorPage404();
        }
        if(!class_exists($controller_name))
        {
            Route::ErrorPage404();
        } else {
            $controller = new $controller_name;
            $action = $action_name;

            if (method_exists($controller, $action))
            {

                if($params)
                {
                    $param = $params[0];
                    $controller->$action($param);
                } else {
                    $controller->$action();
                }
            } else {
                Route::ErrorPage404();
            }
        }
    }

    function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }
}