<?php

 class Model_Auth extends Model{

    private $table = "users";

    public function validate($data)
    {
        //TODO  Add Logic for validation
    }

     public function register($data)
     {
         $logins = map($this->query->getAll('users'),function($user){
             return $user['login'];
         });

         if(in_array($data['login'],$logins))
         {
             return false;
         }

         else {
             $this->query->add('users', [
                 'login' => $data['login'],
                 'password' => md5($data['password'])
             ]);
             return true;
        }
     }

    //TODO: recast this method and move this method to query as generic
     public function login($data)
     {
         $sql ="SELECT * FROM users WHERE login=:login AND password=:password LIMIT 1";

         $statement =  $this->query->pdo->prepare($sql);

         $statement->bindParam(":login",$data['login']);
         $statement->bindParam(":password",$data['password']);
         $statement->execute();
         $user = $statement->fetch(PDO::FETCH_ASSOC);

         if($user) {
             $_SESSION['user'] = $user;
             return true;
         }
         return false;
     }

    public function logout()
    {
        if(!is_null($_SESSION['user']))
            unset($_SESSION['user']);
    }
}