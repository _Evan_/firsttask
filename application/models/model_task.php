<?php

class Model_Task extends Model{

    private $table = "tasks";

    public function validate($data)
    {
        //TODO:  Add Logic for validation
    }

    public function getListModel($sort,$offset,$limit)
    {
        $row = $this->query->getCount($this->table);
        $result =  $this->query->sort($this->table,$sort,$offset,$limit);
        return(array($result,$row['count']));
    }

    public function getTask($id)
    {
       return $this->query->getOne($this->table,$id);
    }

    public function create($data)
    {
        $this->query->add($this->table,$data);
    }

    public function update($id,$postData)
    {
        $data = [
            'id'=>$id,
            'user_name' => $postData['user_name'],
            'title' =>  $postData['title'],
            'content'   =>  $postData['content'],
            'email' =>$postData['email'],
            'completed' =>intval($postData['completed'])
        ];
        $this->query->update($this->table,$data);
    }

    public function  remove($id)
    {
        $this->query->delete($this->table,$id);
    }
}