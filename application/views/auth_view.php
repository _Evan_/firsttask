    <h1 class="form-heading">Login Form</h1>
    <div class="login-form">
        <div class="main-div">
            <form id="Login" method="post" action="/auth/login">
                <div class="form-group">
                    <input name="login" type="text" class="form-control" id="inputEmail" placeholder="Please enter your login">
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control" id="inputPassword" placeholder="Please enter your password">
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
                <a href="/" class="btn btn-primary">Back</a>
            </form>
        </div>
    </div>



