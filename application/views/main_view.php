    <div class="row">
        <div class="col-md-12">
            <h1>Tasks</h1>
            <?php if(!$_SESSION['user']):?>
                <a href="/auth/index" class="btn btn-info">Login</a>
            <?php endif;?>
            <?php if($_SESSION['user']):?>
                <a href="/auth/logout/" class="btn btn-info">Logout</a>
            <?php endif;?>
            <a href="/main/create/" class = "btn btn-success">Add Task</a>
            <h4>Sort by</h4>
            <a href="/main/index/?page=<?=$paginator['currentPage'];?>&&key=user_name" class="btn btn-primary">User name</a>
            <a href="/main/index/?page=<?=$paginator['currentPage'];?>&&key=title" class="btn btn-primary">Title</a>
            <a href="/main/index/?page=<?=$paginator['currentPage'];?>&&key=email" class="btn btn-primary">Email</a>
            <a href="/main/index/?page=<?=$paginator['currentPage'];?>&&key=completed" class="btn btn-primary">Status</a>
            <table class="table">
                <thead>
                <tr>
                    <th>Num</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tbody>
                    <?php foreach($tasks as $task):?>
                        <tr>
                            <td><?= $task['id'];?></td>
                            <td><?= $task['title'];?></td>
                            <td><?= $task['user_name'];?></td>
                            <td><?= $task['completed']==0 ? InProcess : Done; ?></td>
                            <td>
                                <a href="/main/show/<?=$task['id'];?>" class="btn btn-info">Show</a>
                                <?php if($_SESSION['user']):?>
                                    <a href="/main/edit/<?=$task['id'];?>" class="btn btn-warning">
                                        Edit
                                    </a>
                                    <a onclick="return confirm('are you sure?');" href="/main/delete/<?= $task['id'];?>" class="btn btn-danger">
                                        Delete
                                    </a>
                                <?php endif;?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
            <div class="pagination">
                <?php if ($paginator['currentPage'] != 1):?>
                    <a href="/main/index/?page=<?=$paginator['currentPage']-1;?>&&key=<?=$paginator['key'];?>">previous</a>
                <?php endif;?>
                <strong><?=$paginator['currentPage']?></strong>
                <?php if ($paginator['currentPage'] < $paginator['pageCnt']):?>
                    <a href="/main/index/?page=<?=$paginator['currentPage']+1;?>&&key=<?=$paginator['key'];?>">next</a>
                <?php endif;?>
            </div>
        </div>
    </div>