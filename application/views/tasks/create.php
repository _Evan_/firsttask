<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Create Task</h1>
            <form action="/main/store" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" name="user_name" placeholder="Enter your name">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="email" placeholder="Enter your email">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="title" placeholder="Enter the title of your task">
                </div>
                <div class="form-group">
                    <textarea name="content" class="form-control" placeholder="Enter the description of your task"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Submit</button>
                    <a href="/" class="btn btn-primary">Back</a>
                </div>
            </form>
        </div>
    </div>
</div>