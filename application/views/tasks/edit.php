<div class="row">
    <div class="col-md-12">
        <h1>Edit Task</h1>
        <form action="/main/update/<?=$task['id']?>" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="user_name" placeholder="Enter your name" value="<?=$task['user_name']?>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="Enter your email" value="<?=$task['email']?>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="title" placeholder="Enter the title of your task" value="<?=$task['title']?>">
            </div>
            <div class="form-group">
                <textarea name="content" class="form-control" placeholder="Enter the description of your task" ><?=$task['content']?></textarea>
            </div>
            <div class="form-group">
                <label for="completed">Done</label>
                <input type="radio" name="completed" value="1"<?= $task['completed']==1 ? checked : unchecked; ?>>
            </div>
            <div class="form-group">
                <label for="completed">In Process</label>
                <input type="radio" name="completed" value="0"<?= $task['completed']==0 ? checked : unchecked; ?>>
            </div>
            <div class="form-group">
                <button class="btn btn-success" type="submit">Submit</button>
                <a href="/" class="btn btn-primary">Back</a>
            </div>
        </form>
    </div>
</div>